/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
 
    
    audioDeviceManager.initialiseWithDefaultDevices(2,2);
    audioDeviceManager.addAudioCallback(this);
    
    addAndMakeVisible(ampSlider);
    ampSlider.setSliderStyle(juce::Slider::SliderStyle::Rotary);
    ampSlider.setRange(0.f, 1.f);
    
    audioDeviceManager.setMidiInputEnabled ("Impulse  Impulse", true);
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    

    
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeAudioCallback(this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
    
}

void MainComponent::audioDeviceIOCallback (const float** inputChannelData, int numInputChannels,
                                           float** outputChannelData, int numOutputChannels, int numSamples)
{
    const float *inL = inputChannelData[0]; const float *inR = inputChannelData[1]; float *outL = outputChannelData[0]; float *outR = outputChannelData[1];
    while(numSamples--)
    {
//        *outL = *inL * ampSlider.getValue();
//        *outR = *inR * ampSlider.getValue();
        
        *outL = *inL * modWheel;
        *outR = *inR * modWheel;
  
        inL++;
        outL++;
        outR++;
    }
}

void MainComponent::audioDeviceAboutToStart(AudioIODevice* device)
{
    DBG("about to start");
}
void MainComponent::audioDeviceStopped()
{
    DBG("audio stopped");

}

void MainComponent::handleIncomingMidiMessage(MidiInput*, const MidiMessage&)
{
    while (midiMessage.isControllerOfType(1));
    {
        modWheel = midiMessage.getControllerValue();
    }
    modWheel /= 127.f;
    
    
}


void MainComponent::resized()
{
    ampSlider.setBounds(10,10,50,50);
}